package es.unex.giiis.asee.wikileagueapp.testfuncionalcu03;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.giiis.asee.wikileagueapp.MainActivity;
import es.unex.giiis.asee.wikileagueapp.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static java.lang.Thread.sleep;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class AddBuildsTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void AddBuildsTest() throws InterruptedException {

        onView(ViewMatchers.withId(R.id.buildsFragment)).perform(click());
        onView(withId(R.id.CrearBuildButton)).perform(click());
        onView(withId(R.id.crearBuild)).perform(click());

        onView(withId(R.id.titleBuild)).perform(typeText("tituloPrueba"));
        onView(withId(R.id.Champ)).perform(typeText("champPrueba"));
        onView(withId(R.id.Item1)).perform(typeText("item1Prueba"));
        onView(withId(R.id.Item2)).perform(typeText("item2Prueba"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.Item3)).perform(typeText("item3Prueba"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.Item4)).perform(typeText("item4Prueba"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.Item5)).perform(typeText("item5Prueba"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.Item6)).perform(typeText("item6Prueba"));
        Espresso.closeSoftKeyboard();

        onView(withId(R.id.submitBuildButton)).perform(click());
        sleep(2000);

        onView(withId(R.id.buildList)).check(matches(hasDescendant(withId(R.id.titleBuildView))));
        onView(withId(R.id.buildList)).check(matches(hasDescendant(withId(R.id.ChampView))));
        onView(withId(R.id.buildList)).check(matches(hasDescendant(withId(R.id.Item1View))));
        onView(withId(R.id.buildList)).check(matches(hasDescendant(withId(R.id.Item2View))));
        onView(withId(R.id.buildList)).check(matches(hasDescendant(withId(R.id.Item3View))));
        onView(withId(R.id.buildList)).check(matches(hasDescendant(withId(R.id.Item4View))));
        onView(withId(R.id.buildList)).check(matches(hasDescendant(withId(R.id.Item5View))));
        onView(withId(R.id.buildList)).check(matches(hasDescendant(withId(R.id.Item6View))));
    }
}
