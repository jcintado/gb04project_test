package es.unex.giiis.asee.wikileagueapp.testfuncionalcu16;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.matcher.BoundedMatcher;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.giiis.asee.wikileagueapp.Login;
import es.unex.giiis.asee.wikileagueapp.MainActivity;
import es.unex.giiis.asee.wikileagueapp.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static java.lang.Thread.sleep;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class EditCompTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void editCompTest() throws InterruptedException {

        sleep(3000);
        onView(withId(R.id.buildsFragment)).perform(click());
        onView(withId(R.id.CrearCompButton)).perform(click());
        onView(withId(R.id.crearComp)).perform(click());

        onView(withId(R.id.titleComp)).perform(typeText("tituloPrueba"));
        onView(withId(R.id.Champ1)).perform(typeText("champPrueba1"));
        onView(withId(R.id.Champ2)).perform(typeText("champPrueba2"));
        onView(withId(R.id.Champ3)).perform(typeText("champPrueba3"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.Champ4)).perform(typeText("champPrueba4"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.Champ5)).perform(typeText("champPrueba5"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.Comentario)).perform(typeText("comentario prueba"));
        Espresso.closeSoftKeyboard();


        onView(withId(R.id.submitCompButton)).perform(click());
        sleep(2000);

        onView(withId(R.id.compList)).perform(RecyclerViewActions.actionOnItemAtPosition(0, new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return "Click on specific button";
            }

            @Override
            public void perform(UiController uiController, View view) {
                View button = view.findViewById(R.id.botonEditComp);
                button.performClick();
            }
        }));
        onView(withId(R.id.editTitleComp)).perform(clearText(), typeText("tituloPruebaComp"));
        onView(withId(R.id.EditChamp1)).perform(clearText(), typeText("champPrueba1Comps"));
        onView(withId(R.id.EditChamp2)).perform(clearText(), typeText("champPrueba2Comps"));
        onView(withId(R.id.EditChamp3)).perform(clearText(), typeText("champPrueba3Comps"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.EditChamp4)).perform(clearText(), typeText("champPrueba4Comps"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.EditChamp5)).perform(clearText(), typeText("champPrueba5Comps"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.EditComentario)).perform(clearText(), typeText("comentario prueba comps"));
        Espresso.closeSoftKeyboard();

        onView(withId(R.id.submitEditCompButton)).perform(click());
        sleep(2000);

        onView(withId(R.id.compList))
                .perform(RecyclerViewActions.scrollTo(hasDescendant(withText("tituloPruebaComp"))))
                .check(matches(hasItem(hasDescendant(withText("tituloPruebaComp")))));
        onView(withId(R.id.compList)).check(matches(hasItem(hasDescendant(withText("champPrueba1Comps")))));
        onView(withId(R.id.compList)).check(matches(hasItem(hasDescendant(withText("champPrueba2Comps")))));
        onView(withId(R.id.compList)).check(matches(hasItem(hasDescendant(withText("champPrueba3Comps")))));
        onView(withId(R.id.compList)).check(matches(hasItem(hasDescendant(withText("champPrueba4Comps")))));
        onView(withId(R.id.compList)).check(matches(hasItem(hasDescendant(withText("champPrueba5Comps")))));
        onView(withId(R.id.compList)).check(matches(hasItem(hasDescendant(withText("comentario prueba comps")))));


        onView(withId(R.id.compList)).perform(RecyclerViewActions.actionOnItemAtPosition(0, new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return "Click on specific button";
            }

            @Override
            public void perform(UiController uiController, View view) {
                View button = view.findViewById(R.id.botonDeleteComp);
                button.performClick();
            }
        }));

    }

    @Ignore
    public static Matcher<View> hasItem(Matcher<View> matcher) {
        return new BoundedMatcher<View, RecyclerView>(RecyclerView.class) {

            @Override public void describeTo(Description description) {
                description.appendText("has item: ");
                matcher.describeTo(description);
            }

            @Override protected boolean matchesSafely(RecyclerView view) {
                RecyclerView.Adapter adapter = view.getAdapter();
                for (int position = 0; position < adapter.getItemCount(); position++) {
                    int type = adapter.getItemViewType(position);
                    RecyclerView.ViewHolder holder = adapter.createViewHolder(view, type);
                    adapter.onBindViewHolder(holder, position);
                    if (matcher.matches(holder.itemView)) {
                        return true;
                    }
                }
                return false;
            }
        };
    }

}
