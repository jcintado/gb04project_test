package es.unex.giiis.asee.wikileagueapp.testfuncionalcu14;


import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.matcher.BoundedMatcher;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.giiis.asee.wikileagueapp.MainActivity;
import es.unex.giiis.asee.wikileagueapp.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static java.lang.Thread.sleep;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class ModifyDomainTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void modifyDomainTest() throws InterruptedException {


        sleep(3000);
        onView(withId(R.id.champsFragment)).perform(click());
        sleep(3000);

        onView(withId(R.id.champList)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        sleep(500);

        onView(withId(R.id.botonFav)).perform(click());

        Espresso.pressBack();
        onView(withId(R.id.profileFragment)).perform(click());
        onView(withId(R.id.favorites)).perform(click());
        sleep(500);

        onView(withId(R.id.favList)).perform(RecyclerViewActions.actionOnItemAtPosition(0, new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return "Click on specific button";
            }

            @Override
            public void perform(UiController uiController, View view) {
                View button = view.findViewById(R.id.botonEditFav);
                button.performClick();
            }
        }));



        onView(withId(R.id.amateur)).perform(click());
        onView(withId(R.id.guardarMaestriaButton)).perform(click());
        sleep(500);

        onView(withId(R.id.favList))
                .perform(RecyclerViewActions.scrollToPosition(0))
                .check(matches(atPositionOnView(0, withText("Amateur"), R.id.champDomain)));

        onView(withId(R.id.favList)).perform(RecyclerViewActions.actionOnItemAtPosition(0, new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return "Click on specific button";
            }

            @Override
            public void perform(UiController uiController, View view) {
                View button = view.findViewById(R.id.botonEditFav);
                button.performClick();
            }
        }));



        onView(withId(R.id.legendario)).perform(click());
        onView(withId(R.id.guardarMaestriaButton)).perform(click());
        sleep(500);

        onView(withId(R.id.favList))
                .perform(RecyclerViewActions.scrollToPosition(0))
                .check(matches(atPositionOnView(0, withText("Legendario"), R.id.champDomain)));


        onView(withId(R.id.favList)).perform(RecyclerViewActions.actionOnItemAtPosition(0, new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return "Click on specific button";
            }

            @Override
            public void perform(UiController uiController, View view) {
                View button = view.findViewById(R.id.botonDeleteFav);
                button.performClick();
            }
        }));

    }

    @Ignore
    public static Matcher<View> atPositionOnView(final int position, final Matcher<View> itemMatcher,
                                                 @NonNull final int targetViewId) {

        return new BoundedMatcher<View, RecyclerView>(RecyclerView.class) {
            @Override
            public void describeTo(Description description) {
                description.appendText("has view id " + itemMatcher + " at position " + position);
            }

            @Override
            public boolean matchesSafely(final RecyclerView recyclerView) {
                RecyclerView.ViewHolder viewHolder = recyclerView.findViewHolderForAdapterPosition(position);
                View targetView = viewHolder.itemView.findViewById(targetViewId);
                return itemMatcher.matches(targetView);
            }
        };
    }
}
