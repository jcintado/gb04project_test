package es.unex.giiis.asee.wikileagueapp.testfuncionalcu08;


import android.view.View;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;
import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import es.unex.giiis.asee.wikileagueapp.MainActivity;
import es.unex.giiis.asee.wikileagueapp.R;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static java.lang.Thread.sleep;


@LargeTest
@RunWith(AndroidJUnit4.class)
public class DeleteFavChamp {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void deleteFavChamp() throws InterruptedException {

        sleep(3000);
        onView(withId(R.id.champsFragment)).perform(click());
        sleep(3000);

        onView(withId(R.id.champList)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        sleep(500);

        onView(withId(R.id.botonFav)).perform(click());
        Espresso.pressBack();
        onView(withId(R.id.profileFragment)).perform(click());
        onView(withId(R.id.favorites)).perform(click());
        sleep(500);
        onView(withId(R.id.favList)).perform(RecyclerViewActions.actionOnItemAtPosition(0, new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return "Click on specific button";
            }

            @Override
            public void perform(UiController uiController, View view) {
                View button = view.findViewById(R.id.botonDeleteFav);
                button.performClick();
            }
        }));
        sleep(500);
    }
}
