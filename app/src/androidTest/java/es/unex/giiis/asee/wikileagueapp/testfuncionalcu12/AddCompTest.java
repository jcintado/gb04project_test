package es.unex.giiis.asee.wikileagueapp.testfuncionalcu12;




import androidx.test.espresso.Espresso;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.giiis.asee.wikileagueapp.MainActivity;
import es.unex.giiis.asee.wikileagueapp.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static java.lang.Thread.sleep;


@LargeTest
@RunWith(AndroidJUnit4.class)
public class AddCompTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void addCompTest() throws InterruptedException {
        onView(ViewMatchers.withId(R.id.buildsFragment)).perform(click());
        onView(withId(R.id.CrearCompButton)).perform(click());
        onView(withId(R.id.crearComp)).perform(click());

        onView(withId(R.id.titleComp)).perform(typeText("tituloPrueba"));
        Espresso.closeSoftKeyboard();

        onView(withId(R.id.Champ1)).perform(typeText("champ1Prueba"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.Champ2)).perform(typeText("champ2Prueba"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.Champ3)).perform(typeText("champ3Prueba"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.Champ4)).perform(typeText("champ4Prueba"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.Champ5)).perform(typeText("champ5Prueba"));
        Espresso.closeSoftKeyboard();

        onView(withId(R.id.submitCompButton)).perform(click());
        sleep(2000);

        onView(withId(R.id.compList)).check(matches(hasDescendant(withId(R.id.titleCompView))));
        onView(withId(R.id.compList)).check(matches(hasDescendant(withId(R.id.Champ1View))));
        onView(withId(R.id.compList)).check(matches(hasDescendant(withId(R.id.Champ2View))));
        onView(withId(R.id.compList)).check(matches(hasDescendant(withId(R.id.Champ3View))));
        onView(withId(R.id.compList)).check(matches(hasDescendant(withId(R.id.Champ4View))));
        onView(withId(R.id.compList)).check(matches(hasDescendant(withId(R.id.Champ5View))));
        onView(withId(R.id.compList)).check(matches(hasDescendant(withId(R.id.CommentView))));

    }


}
