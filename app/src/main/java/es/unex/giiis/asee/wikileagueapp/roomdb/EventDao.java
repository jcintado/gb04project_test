package es.unex.giiis.asee.wikileagueapp.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import es.unex.giiis.asee.wikileagueapp.model.Comp;
import es.unex.giiis.asee.wikileagueapp.model.Event;

import java.util.List;

@Dao
public interface EventDao {
    @Query("SELECT * FROM events")
    public LiveData<List<Event>> getAll();

    @Insert
    public long insert(Event event);

    @Query("DELETE FROM events")
    public void deleteAll();

}
