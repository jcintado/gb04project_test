package es.unex.giiis.asee.wikileagueapp;

import android.content.Context;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class DetallesCampeonVMFactory extends ViewModelProvider.NewInstanceFactory {
    private final RoomRepository mRepository;
    private final Context context;

    public DetallesCampeonVMFactory(Context context, RoomRepository repository) {
        this.mRepository = repository;
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new DetallesCampeonVM(context, mRepository);
    }
}
