package es.unex.giiis.asee.wikileagueapp;

import android.app.Application;

public class WikiLeague extends Application {
    public AppContainer appContainer;

    @Override
    public void onCreate() {
        super.onCreate();
        appContainer = new AppContainer(this);
    }
}
