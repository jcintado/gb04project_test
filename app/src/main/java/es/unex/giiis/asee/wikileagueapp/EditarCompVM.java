package es.unex.giiis.asee.wikileagueapp;

import android.content.Context;

import androidx.lifecycle.ViewModel;

import es.unex.giiis.asee.wikileagueapp.model.Comp;

public class EditarCompVM extends ViewModel {
    private final RoomRepository mRepository;

    public EditarCompVM(Context context, RoomRepository repository) {
        mRepository = repository.getInstance(context);
    }

    public void update(Comp comp) {
        mRepository.updateComp(comp);
    }
}
