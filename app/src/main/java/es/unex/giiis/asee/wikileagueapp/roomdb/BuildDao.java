package es.unex.giiis.asee.wikileagueapp.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.unex.giiis.asee.wikileagueapp.model.Build;


@Dao
public interface BuildDao {
    @Query("SELECT * FROM builds")
    public LiveData<List<Build>> getAll();
    @Insert
    public long insert(Build build);
    @Delete
    public void delete(Build build);
    @Update
    public void update(Build build);
}

