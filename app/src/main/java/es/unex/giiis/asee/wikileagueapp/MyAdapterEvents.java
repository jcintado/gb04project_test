package es.unex.giiis.asee.wikileagueapp;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import es.unex.giiis.asee.wikileagueapp.model.Comp;
import es.unex.giiis.asee.wikileagueapp.model.Event;

import java.util.ArrayList;
import java.util.List;

public class MyAdapterEvents extends RecyclerView.Adapter<es.unex.giiis.asee.wikileagueapp.MyAdapterEvents.MyViewHolder> {
    private List<Event> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;
        public TextView mTextView2;
        public View mView;

        public Event mItem;

        public MyViewHolder(View v) {
            super(v);
            mView=v;
            mTextView = v.findViewById(R.id.titleView);
            mTextView2 = v.findViewById(R.id.bodyView);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapterEvents() {
        mDataset = new ArrayList<Event>();
    }

    @Override
    public es.unex.giiis.asee.wikileagueapp.MyAdapterEvents.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                                                   int viewType) {
        // create a new view
        // Create new views (invoked by the layout manager)
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.frame_event_list, parent, false);

        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.mItem = mDataset.get(position);
        holder.mTextView.setText(mDataset.get(position).getTitle());
        holder.mTextView2.setText(mDataset.get(position).getBody());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void swap(List<Event> dataset){
        mDataset = dataset;
        notifyDataSetChanged();
    }
}
