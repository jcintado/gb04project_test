package es.unex.giiis.asee.wikileagueapp;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import es.unex.giiis.asee.wikileagueapp.model.Item;
import es.unex.giiis.asee.wikileagueapp.roomdb.WikiLeagueDatabase;

import java.util.ArrayList;
import java.util.List;

public class ItemsFragment extends Fragment implements OnItemsLoadedListener {

    private GridView gridView;
    private MyAdapterItems mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_items, container, false);

        gridView = (GridView) view.findViewById(R.id.itemList);
        mAdapter = new MyAdapterItems(view.getContext(),new ArrayList<Item>());

        AppExecutors.getInstance().networkIO().execute(new ItemsNetworkLoaderRunnable((items) -> mAdapter.swap(items)));

        gridView.setAdapter(mAdapter);

        AppContainer appContainer = ((WikiLeague) getActivity().getApplication()).appContainer;
        ItemsFragmentVM mViewModel = new ViewModelProvider(this, appContainer.factoryItems).get(ItemsFragmentVM.class);
        mViewModel.getItems().observe(getViewLifecycleOwner(), items -> {
            mAdapter.swap(items);
            // Show the repo list or the loading screen based on whether the repos data exists and is loaded
            if (items != null && items.size() != 0) showItemsDataView();
            else showLoading();
        });

        mViewModel.updateItems();

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(),DetallesItem.class);
                Item item = (Item) mAdapter.getItem(position);
                intent.putExtra("name", item.getName());
                intent.putExtra("image", item.getImageUrl());
                intent.putExtra("goldTotal", item.getGoldTotal());
                intent.putExtra("goldSell", item.getGoldSell());
                intent.putExtra("armor", item.getFlatArmorMod());
                intent.putExtra("critChance", item.getFlatCritChanceMod());
                intent.putExtra("hpPool", item.getFlatHpPoolMod());
                intent.putExtra("hpRegen", item.getFlatHpRegenMod());
                intent.putExtra("apDmg", item.getFlatMagicDamageMod());
                intent.putExtra("movementSpeed", item.getFlatMovementSpeedMod());
                intent.putExtra("manaPool", item.getFlatMpPoolMod());
                intent.putExtra("manaRegen", item.getFlatMpRegenMod());
                intent.putExtra("adDmg", item.getFlatPhysicalDamageMod());
                intent.putExtra("goldUpdate", item.getGoldBase());
                intent.putExtra("attackSpeed", item.getPercentAttackSpeedMod());
                intent.putExtra("lifeSteal", item.getPercentLifeStealMod());
                startActivity(intent);
            }
        });

        return view;
    }

    private void showLoading(){
        gridView.setVisibility(View.INVISIBLE);
    }

    private void showItemsDataView(){
        gridView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemsLoaded(List<Item> items) {
        getActivity().runOnUiThread(() -> mAdapter.swap(items));
    }
}