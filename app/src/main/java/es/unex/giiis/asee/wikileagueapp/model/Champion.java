
package es.unex.giiis.asee.wikileagueapp.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName="champs")
public class Champion {

    @SerializedName("armor")
    @Expose
    @ColumnInfo
    private Double armor;
    @SerializedName("armorperlevel")
    @Expose
    @ColumnInfo
    private Double armorperlevel;
    @SerializedName("attackdamage")
    @Expose
    @ColumnInfo
    private Double attackdamage;
    @SerializedName("attackdamageperlevel")
    @Expose
    @ColumnInfo
    private Double attackdamageperlevel;
    @SerializedName("attackrange")
    @Expose
    @ColumnInfo
    private Double attackrange;
    @SerializedName("attackspeedoffset")
    @Expose
    @Ignore
    private Object attackspeedoffset;
    @SerializedName("attackspeedperlevel")
    @Expose
    @ColumnInfo
    private Double attackspeedperlevel;
    @SerializedName("big_image_url")
    @Expose
    @ColumnInfo
    private String bigImageUrl;
    @SerializedName("crit")
    @Expose
    @ColumnInfo
    private Double crit;
    @SerializedName("critperlevel")
    @Expose
    @ColumnInfo
    private Double critperlevel;
    @SerializedName("hp")
    @Expose
    @ColumnInfo
    private Double hp;
    @SerializedName("hpperlevel")
    @Expose
    @ColumnInfo
    private Double hpperlevel;
    @SerializedName("hpregen")
    @Expose
    @ColumnInfo
    private Double hpregen;
    @SerializedName("hpregenperlevel")
    @Expose
    @ColumnInfo
    private Double hpregenperlevel;
    @SerializedName("id")
    @Expose
    @PrimaryKey
    private Integer id;
    @SerializedName("image_url")
    @Expose
    @ColumnInfo
    private String imageUrl;
    @SerializedName("movespeed")
    @Expose
    @ColumnInfo
    private Double movespeed;
    @SerializedName("mp")
    @Expose
    @ColumnInfo
    private Double mp;
    @SerializedName("mpperlevel")
    @Expose
    @ColumnInfo
    private Double mpperlevel;
    @SerializedName("mpregen")
    @Expose
    @ColumnInfo
    private Double mpregen;
    @SerializedName("mpregenperlevel")
    @Expose
    @ColumnInfo
    private Double mpregenperlevel;
    @SerializedName("name")
    @Expose
    @ColumnInfo
    private String name;
    @SerializedName("spellblock")
    @Expose
    @ColumnInfo
    private Double spellblock;
    @SerializedName("spellblockperlevel")
    @Expose
    @ColumnInfo
    private Double spellblockperlevel;
    @SerializedName("videogame_versions")
    @Expose
    @Ignore
    private List<String> videogameVersions = null;

    public Double getArmor() {
        return armor;
    }

    public void setArmor(Double armor) {
        this.armor = armor;
    }

    public Double getArmorperlevel() {
        return armorperlevel;
    }

    public void setArmorperlevel(Double armorperlevel) {
        this.armorperlevel = armorperlevel;
    }

    public Double getAttackdamage() {
        return attackdamage;
    }

    public void setAttackdamage(Double attackdamage) {
        this.attackdamage = attackdamage;
    }

    public Double getAttackdamageperlevel() {
        return attackdamageperlevel;
    }

    public void setAttackdamageperlevel(Double attackdamageperlevel) {
        this.attackdamageperlevel = attackdamageperlevel;
    }

    public Double getAttackrange() {
        return attackrange;
    }

    public void setAttackrange(Double attackrange) {
        this.attackrange = attackrange;
    }

    public Object getAttackspeedoffset() {
        return attackspeedoffset;
    }

    public void setAttackspeedoffset(Object attackspeedoffset) {
        this.attackspeedoffset = attackspeedoffset;
    }

    public Double getAttackspeedperlevel() {
        return attackspeedperlevel;
    }

    public void setAttackspeedperlevel(Double attackspeedperlevel) {
        this.attackspeedperlevel = attackspeedperlevel;
    }

    public String getBigImageUrl() {
        return bigImageUrl;
    }

    public void setBigImageUrl(String bigImageUrl) {
        this.bigImageUrl = bigImageUrl;
    }

    public Double getCrit() {
        return crit;
    }

    public void setCrit(Double crit) {
        this.crit = crit;
    }

    public Double getCritperlevel() {
        return critperlevel;
    }

    public void setCritperlevel(Double critperlevel) {
        this.critperlevel = critperlevel;
    }

    public Double getHp() {
        return hp;
    }

    public void setHp(Double hp) {
        this.hp = hp;
    }

    public Double getHpperlevel() {
        return hpperlevel;
    }

    public void setHpperlevel(Double hpperlevel) {
        this.hpperlevel = hpperlevel;
    }

    public Double getHpregen() {
        return hpregen;
    }

    public void setHpregen(Double hpregen) {
        this.hpregen = hpregen;
    }

    public Double getHpregenperlevel() {
        return hpregenperlevel;
    }

    public void setHpregenperlevel(Double hpregenperlevel) {
        this.hpregenperlevel = hpregenperlevel;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Double getMovespeed() {
        return movespeed;
    }

    public void setMovespeed(Double movespeed) {
        this.movespeed = movespeed;
    }

    public Double getMp() {
        return mp;
    }

    public void setMp(Double mp) {
        this.mp = mp;
    }

    public Double getMpperlevel() {
        return mpperlevel;
    }

    public void setMpperlevel(Double mpperlevel) {
        this.mpperlevel = mpperlevel;
    }

    public Double getMpregen() {
        return mpregen;
    }

    public void setMpregen(Double mpregen) {
        this.mpregen = mpregen;
    }

    public Double getMpregenperlevel() {
        return mpregenperlevel;
    }

    public void setMpregenperlevel(Double mpregenperlevel) {
        this.mpregenperlevel = mpregenperlevel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getSpellblock() {
        return spellblock;
    }

    public void setSpellblock(Double spellblock) {
        this.spellblock = spellblock;
    }

    public Double getSpellblockperlevel() {
        return spellblockperlevel;
    }

    public void setSpellblockperlevel(Double spellblockperlevel) {
        this.spellblockperlevel = spellblockperlevel;
    }

    public List<String> getVideogameVersions() {
        return videogameVersions;
    }

    public void setVideogameVersions(List<String> videogameVersions) {
        this.videogameVersions = videogameVersions;
    }

}
