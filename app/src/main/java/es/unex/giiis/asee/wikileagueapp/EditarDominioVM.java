package es.unex.giiis.asee.wikileagueapp;

import android.content.Context;

import androidx.lifecycle.ViewModel;

import es.unex.giiis.asee.wikileagueapp.model.FavChamp;

public class EditarDominioVM extends ViewModel {
    private final RoomRepository mRepository;

    public EditarDominioVM(Context context, RoomRepository repository) {
        mRepository = repository.getInstance(context);
    }

    public void update(FavChamp favChamp) {
        mRepository.updateFavChamp(favChamp);
    }
}
