package es.unex.giiis.asee.wikileagueapp;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.wikileagueapp.model.Item;

public class ItemsFragmentVM extends ViewModel {
    private final ApiRepository mRepository;
    private final LiveData<List<Item>> mItems;

    public ItemsFragmentVM(ApiRepository repository) {
        mRepository = repository;
        mItems = mRepository.getCurrentItems();
    }

    public void updateItems(){
        mRepository.updateItems();
    }

    public void onRefresh() {
        mRepository.doFetchItems();
    }

    public LiveData<List<Item>> getItems() {
        return mItems;
    }
}
