package es.unex.giiis.asee.wikileagueapp;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.wikileagueapp.model.Build;
import es.unex.giiis.asee.wikileagueapp.model.FavChamp;

public class ListaFavoritosVM extends ViewModel {
    private final RoomRepository mRepository;
    private final LiveData<List<FavChamp>> mFavs;

    public ListaFavoritosVM(Context context, RoomRepository repository) {
        mRepository = repository.getInstance(context);
        mFavs = mRepository.getAllFavs();
    }

    public LiveData<List<FavChamp>> getAllFavs() {
        return mFavs;
    }

    public void delete(FavChamp favChamp) {
        mRepository.deleteFavChamp(favChamp);
    }
}
