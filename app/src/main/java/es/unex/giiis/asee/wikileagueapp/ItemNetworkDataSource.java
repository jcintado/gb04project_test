package es.unex.giiis.asee.wikileagueapp;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import es.unex.giiis.asee.wikileagueapp.model.Item;

public class ItemNetworkDataSource {
    private static final String LOG_TAG = ItemNetworkDataSource.class.getSimpleName();
    private static ItemNetworkDataSource sInstance;

    // LiveData storing the latest downloaded weather forecasts
    private final MutableLiveData<Item[]> mDownloadedItems;

    private ItemNetworkDataSource() {
        mDownloadedItems = new MutableLiveData<>();
    }

    public synchronized static ItemNetworkDataSource getInstance() {
        Log.d(LOG_TAG, "Getting the network data source");
        if (sInstance == null) {
            sInstance = new ItemNetworkDataSource();
            Log.d(LOG_TAG, "Made new network data source");
        }
        return sInstance;
    }

    public LiveData<Item[]> getCurrentItems() {
        return mDownloadedItems;
    }

    /**
     * Gets the newest champs
     */
    public void fetchItems() {
        Log.d(LOG_TAG, "Fetch items started");
        // Get data from network and pass it to LiveData
        AppExecutors.getInstance().networkIO().execute(new ItemsNetworkLoaderRunnable(items -> mDownloadedItems.postValue(items.toArray(new Item[0]))));
    }
}
