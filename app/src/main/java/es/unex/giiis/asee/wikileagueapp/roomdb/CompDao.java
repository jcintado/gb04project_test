package es.unex.giiis.asee.wikileagueapp.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import java.util.List;
import es.unex.giiis.asee.wikileagueapp.model.Comp;
import es.unex.giiis.asee.wikileagueapp.model.Event;
@Dao
public interface CompDao {
    @Query("SELECT * FROM comps")
    public LiveData<List<Comp>> getAll();

    @Insert
    public long insert(Comp comp);

    @Delete
    public void delete(Comp comp);

    @Update
    public void update(Comp comp);
}

