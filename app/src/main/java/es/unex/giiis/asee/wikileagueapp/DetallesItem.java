package es.unex.giiis.asee.wikileagueapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetallesItem extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_item);

        String name = getIntent().getStringExtra("name");
        String image = getIntent().getStringExtra("image");
        int goldTotal = getIntent().getIntExtra("goldTotal", 0);
        int goldSell = getIntent().getIntExtra("goldSell", 0);
        int armor = getIntent().getIntExtra("armor", 0);
        int critChance = getIntent().getIntExtra("critChance", 0);
        int hpPool = getIntent().getIntExtra("hpPool", 0);
        int hpRegen = getIntent().getIntExtra("hpRegen", 0);
        int apDmg = getIntent().getIntExtra("apDmg", 0);
        int movementSpeed = getIntent().getIntExtra("movementSpeed", 0);
        int manaPool = getIntent().getIntExtra("manaPool", 0);
        int manaRegen = getIntent().getIntExtra("manaRegen", 0);
        int adDmg = getIntent().getIntExtra("adDmg", 0);
        int goldUpdate = getIntent().getIntExtra("goldUpdate", 0);
        int attackSpeed = getIntent().getIntExtra("attackSpeed", 0);
        int lifeSteal = getIntent().getIntExtra("lifeSteal", 0);

        TextView nombre = findViewById(R.id.nombreItem);
        nombre.setText(name);
        ImageView imagen = findViewById(R.id.imagenItem);
        Picasso.get()
                .load(image)
                .fit()
                .centerCrop()
                .into(imagen);
        TextView precio = findViewById(R.id.precio);
        precio.setText("Precio: "+goldTotal);

        TextView precioVenta = findViewById(R.id.precioVenta);
        precioVenta.setText("Precio de venta: "+goldSell);

        TextView armadura = findViewById(R.id.armadura);
        armadura.setText("Armadura: "+armor);

        TextView proCritico = findViewById(R.id.proCritico);
        proCritico.setText("Prob de crítico: "+critChance);

        TextView vida = findViewById(R.id.vida);
        vida.setText("Vida: "+hpPool);

        TextView vidaRegen = findViewById(R.id.vidaRegen);
        vidaRegen.setText("Regen vida: "+hpRegen);

        TextView danioMagico = findViewById(R.id.ap);
        danioMagico.setText("Daño magico: "+apDmg);

        TextView velocidad = findViewById(R.id.velocidad);
        velocidad.setText("Velocidad: "+movementSpeed);

        TextView mana = findViewById(R.id.mana);
        mana.setText("Mana: "+manaPool);

        TextView manaReg = findViewById(R.id.manaRegen);
        manaReg.setText("Regen de mana: "+manaRegen);

        TextView danioFisico = findViewById(R.id.ad);
        danioFisico.setText("Daño fisico: "+adDmg);

        TextView oroMejora = findViewById(R.id.oroMejora);
        oroMejora.setText("Oro de mejora: "+goldUpdate);

        TextView velocidadAtaque = findViewById(R.id.velAtaque);
        velocidadAtaque.setText("Velocidad de ataque: "+adDmg);

        TextView roboVida = findViewById(R.id.roboVida);
        roboVida.setText("Robo de vida: "+lifeSteal);
    }
}
