package es.unex.giiis.asee.wikileagueapp;

import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.Arrays;
import java.util.List;

import es.unex.giiis.asee.wikileagueapp.model.Champion;
import es.unex.giiis.asee.wikileagueapp.model.Item;
import es.unex.giiis.asee.wikileagueapp.roomdb.ChampDao;
import es.unex.giiis.asee.wikileagueapp.roomdb.ItemDao;

public class ApiRepository {
    private static final String LOG_TAG = ApiRepository.class.getSimpleName();

    // For Singleton instantiation
    private static ApiRepository sInstance;
    private final ChampDao mChampDao;
    private final ItemDao mItemDao;
    private final ChampNetworkDataSource mChampNetworkDataSource;
    private final ItemNetworkDataSource mItemNetworkDataSource;
    private final AppExecutors mExecutors = AppExecutors.getInstance();
    private Long champsLastUpdateTimeMillisMap;
    private Long itemsLastUpdateTimeMillisMap;
    private static final long MIN_TIME_FROM_LAST_FETCH_MILLIS = 60000;

    private ApiRepository(ChampDao champDao, ItemDao itemDao, ChampNetworkDataSource champNetworkDataSource, ItemNetworkDataSource itemNetworkDataSource) {
        mChampDao = champDao;
        mItemDao = itemDao;
        mChampNetworkDataSource = champNetworkDataSource;
        mItemNetworkDataSource = itemNetworkDataSource;

        LiveData<Champion[]> networkData = mChampNetworkDataSource.getCurrentChamps();
        LiveData<Item[]> itemNetworkData = mItemNetworkDataSource.getCurrentItems();
        // As long as the repository exists, observe the network LiveData.
        // If that LiveData changes, update the database.
        networkData.observeForever(newChampsFromNetwork -> {
            mExecutors.diskIO().execute(() -> {
                if (newChampsFromNetwork.length > 0){
                    mChampDao.deleteAll();
                }
                // Insert our new repos into local database
                mChampDao.bulkInsert(Arrays.asList(newChampsFromNetwork));
                Log.d(LOG_TAG, "New champs inserted in Room");
            });
        });
        itemNetworkData.observeForever(newItemsFromNetwork -> {
            mExecutors.diskIO().execute(() -> {
                if (newItemsFromNetwork.length > 0){
                    mItemDao.deleteAll();
                }
                mItemDao.bulkInsert(Arrays.asList(newItemsFromNetwork));
                Log.d(LOG_TAG, "New items inserted in Room");
            });
        });
    }

    public static synchronized ApiRepository getInstance(ChampDao dao, ItemDao dao2, ChampNetworkDataSource nds, ItemNetworkDataSource nds2) {
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            sInstance = new ApiRepository(dao, dao2, nds, nds2);
            Log.d(LOG_TAG, "Made new repository");
        }
        return sInstance;
    }

    public void updateRepo(){
        AppExecutors.getInstance().diskIO().execute(() -> {
            if (isFetchNeeded()) {
                doFetchChamps();
            }
        });
    }

    public void updateItems(){
        AppExecutors.getInstance().diskIO().execute(() -> {
            if (isFetchNeededItems()) {
                doFetchItems();
            }
        });
    }

    public void doFetchChamps(){
        Log.d(LOG_TAG, "Fetching Champs from API");
        AppExecutors.getInstance().diskIO().execute(() -> {
            mChampDao.deleteAll();
            mChampNetworkDataSource.fetchChamps();
            champsLastUpdateTimeMillisMap=System.currentTimeMillis();
        });
    }

    public void doFetchItems(){
        Log.d(LOG_TAG, "Fetching Items from API");
        AppExecutors.getInstance().diskIO().execute(() -> {
            mItemDao.deleteAll();
            mItemNetworkDataSource.fetchItems();
            itemsLastUpdateTimeMillisMap=System.currentTimeMillis();
        });
    }

    /**
     * Database related operations
     **/

    public LiveData<List<Champion>> getCurrentChamps() {
        return mChampDao.getAll();
    }

    public LiveData<List<Item>> getCurrentItems() {
        return mItemDao.getAll();
    }

    /**
     * Checks if we have to update the champs data.
     * @return Whether a fetch is needed
     */
    private boolean isFetchNeeded() {
        Long lastFetchTimeMillis = champsLastUpdateTimeMillisMap;
        lastFetchTimeMillis = lastFetchTimeMillis == null ? 0L : lastFetchTimeMillis;
        long timeFromLastFetch = System.currentTimeMillis() - lastFetchTimeMillis;
        // Implement cache policy: When time has passed or no repos in cache
        return mChampDao.getNumberChampsInCache() == 0 || timeFromLastFetch > MIN_TIME_FROM_LAST_FETCH_MILLIS;
    }

    /**
     * Checks if we have to update the items data.
     * @return Whether a fetch is needed
     */
    private boolean isFetchNeededItems() {
        Long lastFetchTimeMillis = itemsLastUpdateTimeMillisMap;
        lastFetchTimeMillis = lastFetchTimeMillis == null ? 0L : lastFetchTimeMillis;
        long timeFromLastFetch = System.currentTimeMillis() - lastFetchTimeMillis;
        // Implement cache policy: When time has passed or no repos in cache
        return mItemDao.getNumberItemsInCache() == 0 || timeFromLastFetch > MIN_TIME_FROM_LAST_FETCH_MILLIS;
    }
}
