package es.unex.giiis.asee.wikileagueapp;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


public class Registro extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        setup();
    }

    private void setup() {
        Button signUpButton = findViewById(R.id.signUpButton2);
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText eTEmail = findViewById(R.id.emailEditText2);
                EditText eTPassword = findViewById(R.id.passwordEditText2);
                EditText eTPassword2 = findViewById(R.id.passwordEditText3);
                if (!eTEmail.getText().toString().equals("") && !eTPassword.getText().toString().equals("")) {
                    if (!eTPassword.getText().toString().equals(eTPassword2.getText().toString())) {
                        Log.w("PassError", "createUserWithEmail:failure");
                        Toast.makeText(Registro.this, "Las contraseñas no coinciden.",
                                Toast.LENGTH_SHORT).show();
                    }
                    else {
                        FirebaseAuth mAuth = FirebaseAuth.getInstance();
                        mAuth.createUserWithEmailAndPassword(eTEmail.getText().toString(), eTPassword.getText().toString()).addOnCompleteListener(Registro.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    Log.d("Success", "createUserWithEmail:success");
                                    Intent intent = new Intent(Registro.this, MainActivity.class);
                                    startActivity(intent);
                                } else {
                                    // If sign in fails, display a message to the user.
                                    Log.w("Error", "createUserWithEmail:failure", task.getException());
                                    Toast.makeText(Registro.this, "Autenticación fallida.",
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                }
                else {
                    Log.w("FieldsError", "createUserWithEmail:failure");
                    Toast.makeText(Registro.this, "Los campos deben ser rellenados.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}