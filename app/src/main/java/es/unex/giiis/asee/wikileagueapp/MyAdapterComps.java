package es.unex.giiis.asee.wikileagueapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.wikileagueapp.model.Build;
import es.unex.giiis.asee.wikileagueapp.model.Comp;

public class MyAdapterComps extends RecyclerView.Adapter<es.unex.giiis.asee.wikileagueapp.MyAdapterComps.MyCompViewHolder> {

    public interface OnCompListDeleteInteractionListener{
        public void onCompListDeleteInteraction(Comp comp, int position);
    }

    public MyAdapterComps.OnCompListDeleteInteractionListener mDeleteListener;

    public interface OnCompListEditInteractionListener{
        public void onCompListEditInteraction(Comp comp, int position);
    }

    public MyAdapterComps.OnCompListEditInteractionListener mEditListener;

    private List<Comp> mDataset;

    public static class MyCompViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView titleCompView;
        public TextView Champ1View;
        public TextView Champ2View;
        public TextView Champ3View;
        public TextView Champ4View;
        public TextView Champ5View;
        public TextView CommentView;
        public ImageButton mDeleteButton;
        public ImageButton mEditButton;

        public View mView;

        public Comp mItem;

        public MyCompViewHolder(View v) {
            super(v);
            mView=v;
            titleCompView = v.findViewById(R.id.titleCompView);
            Champ1View = v.findViewById(R.id.Champ1View);
            Champ2View = v.findViewById(R.id.Champ2View);
            Champ3View = v.findViewById(R.id.Champ3View);
            Champ4View = v.findViewById(R.id.Champ4View);
            Champ5View = v.findViewById(R.id.Champ5View);
            CommentView = v.findViewById(R.id.CommentView);
            mDeleteButton = v.findViewById(R.id.botonDeleteComp);
            mEditButton = v.findViewById(R.id.botonEditComp);
        }
    }

    public MyAdapterComps(MyAdapterComps.OnCompListDeleteInteractionListener deleteListener, MyAdapterComps.OnCompListEditInteractionListener editListener) {

        mDataset = new ArrayList<Comp>();
        mDeleteListener = deleteListener;
        mEditListener = editListener;
    }

    @Override
    public es.unex.giiis.asee.wikileagueapp.MyAdapterComps.MyCompViewHolder onCreateViewHolder(ViewGroup parent,
                                                                                               int viewType) {
        // create a new view
        // Create new views (invoked by the layout manager)
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.frame_comp_list, parent, false);

        return new MyCompViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyCompViewHolder holder, int position) {
        holder.mItem = mDataset.get(position);
        holder.titleCompView.setText(mDataset.get(position).getTitle());
        holder.Champ1View.setText(mDataset.get(position).getChamp1());
        holder.Champ2View.setText(mDataset.get(position).getChamp2());
        holder.Champ3View.setText(mDataset.get(position).getChamp3());
        holder.Champ4View.setText(mDataset.get(position).getChamp4());
        holder.Champ5View.setText(mDataset.get(position).getChamp5());
        holder.CommentView.setText(mDataset.get(position).getComment());
        holder.mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDeleteListener.onCompListDeleteInteraction(holder.mItem, position);
            }
        });
        holder.mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditListener.onCompListEditInteraction(holder.mItem, position);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void swap(List<Comp> dataset){
        mDataset = dataset;
        notifyDataSetChanged();
    }
}
