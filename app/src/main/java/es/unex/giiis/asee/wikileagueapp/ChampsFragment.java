package es.unex.giiis.asee.wikileagueapp;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import es.unex.giiis.asee.wikileagueapp.model.Champion;
import es.unex.giiis.asee.wikileagueapp.roomdb.WikiLeagueDatabase;

import java.util.ArrayList;
import java.util.List;

public class ChampsFragment extends Fragment implements MyAdapterChamps.OnListInteractionListener {

    private RecyclerView recyclerView;
    private MyAdapterChamps mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_champs, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.champList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        mAdapter = new MyAdapterChamps(new ArrayList<Champion>(), this);
        recyclerView.setAdapter(mAdapter);

        AppContainer appContainer = ((WikiLeague) getActivity().getApplication()).appContainer;
        ChampsFragmentVM mViewModel = new ViewModelProvider(this, appContainer.factory).get(ChampsFragmentVM.class);
        mViewModel.getChamps().observe(getViewLifecycleOwner(), champs -> {
            mAdapter.swap(champs);
            // Show the repo list or the loading screen based on whether the repos data exists and is loaded
            if (champs != null && champs.size() != 0) showChampsDataView();
            else showLoading();
        });

        mViewModel.updateRepo();

        return view;
    }

    private void showLoading(){
        recyclerView.setVisibility(View.INVISIBLE);
    }

    private void showChampsDataView(){
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onListInteraction(Champion champ) {
        Double armor = champ.getArmor();
        Double ad = champ.getAttackdamage();
        Double attackrange = champ.getAttackrange();
        String splashartUrl = champ.getBigImageUrl();
        Double crit = champ.getCrit();
        Double hp = champ.getHp();
        Double hpregen = champ.getHpregen();
        Double movespeed = champ.getMovespeed();
        Double mana = champ.getMp();
        Double manaregen = champ.getMpregen();
        String name = champ.getName();
        Double spellblock = champ.getSpellblock();
        String imageUrl = champ.getImageUrl();
        Intent intent = new Intent (getActivity(), DetallesCampeon.class);
        intent.putExtra("armor", armor);
        intent.putExtra("ad", ad);
        intent.putExtra("range", attackrange);
        intent.putExtra("splashart", splashartUrl);
        intent.putExtra("crit", crit);
        intent.putExtra("hp", hp);
        intent.putExtra("hpregen", hpregen);
        intent.putExtra("movespeed", movespeed);
        intent.putExtra("mana", mana);
        intent.putExtra("manaregen", manaregen);
        intent.putExtra("name", name);
        intent.putExtra("spellblock", spellblock);
        intent.putExtra("image", imageUrl);
        startActivity(intent);
    }

}
