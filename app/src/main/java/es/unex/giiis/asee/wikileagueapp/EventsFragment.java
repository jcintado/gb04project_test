package es.unex.giiis.asee.wikileagueapp;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import es.unex.giiis.asee.wikileagueapp.model.Event;
import es.unex.giiis.asee.wikileagueapp.roomdb.WikiLeagueDatabase;

import static android.app.Activity.RESULT_OK;

public class EventsFragment extends Fragment {

    private static final int ADD_EVENT_REQUEST = 101;

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private MyAdapterEvents mAdapter;
    EventsFragmentVM mViewModel;
    AppContainer appContainer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        appContainer = ((WikiLeague) getActivity().getApplication()).appContainer;
        mViewModel = new ViewModelProvider(this, appContainer.factoryEventsFragment).get(EventsFragmentVM.class);

        View view = inflater.inflate(R.layout.fragment_events, container, false);
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), CrearEvento.class);
                startActivityForResult(intent,ADD_EVENT_REQUEST);
            }
        });

        FloatingActionButton fab2 = (FloatingActionButton) view.findViewById(R.id.clear);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mViewModel.deleteAll();
                        getActivity().runOnUiThread(() -> Toast.makeText(getActivity(), "Se han borrado todas las notas.", Toast.LENGTH_SHORT).show());
                    }
                });
            }
        });

        mRecyclerView = (RecyclerView) view.findViewById(R.id.eventList);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(view.getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MyAdapterEvents();
        mRecyclerView.setAdapter(mAdapter);

        mViewModel.getAllEvents().observe(getViewLifecycleOwner(), events -> {
            mAdapter.swap(events);
            if (events != null && events.size() != 0) showEventsDataView();
            else showLoading();
        });

        return view;
    }

    private void showLoading(){
        mRecyclerView.setVisibility(View.INVISIBLE);
    }

    private void showEventsDataView(){
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);

        if (requestCode == ADD_EVENT_REQUEST){
            if (resultCode == RESULT_OK){
                Event event = new Event(data);
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mViewModel.insert(event);
                    }
                });
            }
        }
    }

}