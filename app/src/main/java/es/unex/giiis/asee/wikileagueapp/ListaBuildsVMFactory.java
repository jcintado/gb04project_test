package es.unex.giiis.asee.wikileagueapp;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class ListaBuildsVMFactory extends ViewModelProvider.NewInstanceFactory {
    private final RoomRepository mRepository;
    private final Context context;

    public ListaBuildsVMFactory(Context context, RoomRepository repository) {
        this.mRepository = repository;
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new ListaBuildsVM(context, mRepository);
    }
}
