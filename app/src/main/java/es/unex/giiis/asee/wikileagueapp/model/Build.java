package es.unex.giiis.asee.wikileagueapp.model;

import android.content.Intent;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
@Entity(tableName = "builds")
public class Build {
    @Ignore
    public static final String BUILD_SEP = System.getProperty("line.separator");
    @Ignore
    public final static String ID = "ID";
    @Ignore
    public final static String TITLE = "title";
    @Ignore
    public final static String CHAMP = "champ";
    @Ignore
    public final static String ITEM1 = "item1";
    @Ignore
    public final static String ITEM2 = "item2";
    @Ignore
    public final static String ITEM3 = "item3";
    @Ignore
    public final static String ITEM4 = "item4";
    @Ignore
    public final static String ITEM5 = "item5";
    @Ignore
    public final static String ITEM6 = "item6";

    @PrimaryKey(autoGenerate = true)
    private long id;
    @ColumnInfo(name = "title")
    private String title = new String();
    @ColumnInfo(name = "champ")
    private String champ = new String();
    @ColumnInfo(name = "item1")
    private String item1 = new String();
    @ColumnInfo(name = "item2")
    private String item2 = new String();
    @ColumnInfo(name = "item3")
    private String item3 = new String();
    @ColumnInfo(name = "item4")
    private String item4 = new String();
    @ColumnInfo(name = "item5")
    private String item5 = new String();
    @ColumnInfo(name = "item6")
    private String item6 = new String();

    @Ignore
    Build(String title, String champ, String item1, String item2, String item3, String item4, String item5, String item6) {
        this.title = title;
        this.champ = champ;
        this.item1 = item1;
        this.item2 = item2;
        this.item3 = item3;
        this.item4 = item4;
        this.item5 = item5;
        this.item6 = item6;
    }

    @Ignore
    public Build(Intent intent) {
        id = intent.getLongExtra(Build.ID,0);
        title = intent.getStringExtra(Build.TITLE);
        champ = intent.getStringExtra(Build.CHAMP);
        item1 = intent.getStringExtra(Build.ITEM1);
        item2 = intent.getStringExtra(Build.ITEM2);
        item3 = intent.getStringExtra(Build.ITEM3);
        item4 = intent.getStringExtra(Build.ITEM4);
        item5 = intent.getStringExtra(Build.ITEM5);
        item6 = intent.getStringExtra(Build.ITEM6);
    }

    public Build(long id, String title, String champ, String item1, String item2, String item3, String item4, String item5, String item6) {
        this.id = id;
        this.title = title;
        this.champ = champ;
        this.item1 = item1;
        this.item2 = item2;
        this.item3 = item3;
        this.item4 = item4;
        this.item5 = item5;
        this.item6 = item6;
    }

    public static void packageIntent(Intent intent, String title, String champ, String item1, String item2, String item3, String item4, String item5, String item6) {

        intent.putExtra(Build.TITLE, title);
        intent.putExtra(Build.CHAMP, champ);
        intent.putExtra(Build.ITEM1, item1);
        intent.putExtra(Build.ITEM2, item2);
        intent.putExtra(Build.ITEM3, item3);
        intent.putExtra(Build.ITEM4, item4);
        intent.putExtra(Build.ITEM5, item5);
        intent.putExtra(Build.ITEM6, item6);
    }

    public String toString() {
        return id + BUILD_SEP + title + BUILD_SEP + champ + BUILD_SEP + item1 + BUILD_SEP + item2 + BUILD_SEP + item3 + BUILD_SEP + item4 + BUILD_SEP + item5 + BUILD_SEP + item6;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getChamp() {
        return champ;
    }

    public void setChamp(String champ) {
        this.champ = champ;
    }

    public String getItem1() {
        return item1;
    }

    public void setItem1(String item1) {
        this.item1 = item1;
    }

    public String getItem2() {
        return item2;
    }

    public void setItem2(String item2) {
        this.item2 = item2;
    }

    public String getItem3() {
        return item3;
    }

    public void setItem3(String item3) {
        this.item3 = item3;
    }

    public String getItem4() {
        return item4;
    }

    public void setItem4(String item4) {
        this.item4 = item4;
    }

    public String getItem5() {
        return item5;
    }

    public void setItem5(String item5) {
        this.item5 = item5;
    }

    public String getItem6() {
        return item6;
    }

    public void setItem6(String item6) {
        this.item6 = item6;
    }
}
