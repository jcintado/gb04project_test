package es.unex.giiis.asee.wikileagueapp;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.wikileagueapp.model.Comp;

public class ListaCompsVM extends ViewModel {
    private final RoomRepository mRepository;
    private final LiveData<List<Comp>> mComps;

    public ListaCompsVM(Context context, RoomRepository repository) {
        mRepository = repository.getInstance(context);
        mComps = mRepository.getAllComps();
    }

    public LiveData<List<Comp>> getAllComps() {
        return mComps;
    }

    public void delete(Comp comp) {
        mRepository.deleteComp(comp);
    }

    public void insert(Comp comp) {
        mRepository.insertComp(comp);
    }
}
