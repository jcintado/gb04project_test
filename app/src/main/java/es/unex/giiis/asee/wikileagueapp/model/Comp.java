package es.unex.giiis.asee.wikileagueapp.model;

import android.content.Intent;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity (tableName = "comps")
public class Comp {

    @Ignore
    public static final String COM_SEP = System.getProperty("line.separator");
    @Ignore
    public final static String ID = "ID";
    @Ignore
    public final static String TITLE = "title";
    @Ignore
    public final static String CHAMP1 = "champ1";
    @Ignore
    public final static String CHAMP2 = "champ2";
    @Ignore
    public final static String CHAMP3 = "champ3";
    @Ignore
    public final static String CHAMP4 = "champ4";
    @Ignore
    public final static String CHAMP5 = "champ5";
    @Ignore
    public final static String COMMENT = "comment";

    @PrimaryKey(autoGenerate = true)
    private long id;
    @ColumnInfo(name = "title")
    private String title = new String();
    @ColumnInfo(name = "champ1")
    private String champ1 = new String();
    @ColumnInfo(name = "champ2")
    private String champ2 = new String();
    @ColumnInfo(name = "champ3")
    private String champ3 = new String();
    @ColumnInfo(name = "champ4")
    private String champ4 = new String();
    @ColumnInfo(name = "champ5")
    private String champ5 = new String();
    @ColumnInfo(name = "comment")
    private String comment = new String();

    @Ignore
    Comp(String title, String champ1, String champ2, String champ3, String champ4, String champ5, String comment) {
        this.title = title;
        this.champ1 = champ1;
        this.champ2 = champ2;
        this.champ3 = champ3;
        this.champ4 = champ4;
        this.champ5 = champ5;
        this.comment = comment;
    }

    @Ignore
    public Comp(Intent intent) {
        id = intent.getLongExtra(Comp.ID,0);
        title = intent.getStringExtra(Comp.TITLE);
        champ1 = intent.getStringExtra(Comp.CHAMP1);
        champ2 = intent.getStringExtra(Comp.CHAMP2);
        champ3 = intent.getStringExtra(Comp.CHAMP3);
        champ4 = intent.getStringExtra(Comp.CHAMP4);
        champ5 = intent.getStringExtra(Comp.CHAMP5);
        comment = intent.getStringExtra(Comp.COMMENT);
    }

    public Comp(long id, String title, String champ1, String champ2, String champ3, String champ4, String champ5, String comment) {
        this.id = id;
        this.title = title;
        this.champ1 = champ1;
        this.champ2 = champ2;
        this.champ3 = champ3;
        this.champ4 = champ4;
        this.champ5 = champ5;
        this.comment = comment;
    }

    public static void packageIntent(Intent intent, String title, String champ1, String champ2, String champ3, String champ4, String champ5, String comment) {

        intent.putExtra(Comp.TITLE, title);
        intent.putExtra(Comp.CHAMP1, champ1);
        intent.putExtra(Comp.CHAMP2, champ2);
        intent.putExtra(Comp.CHAMP3, champ3);
        intent.putExtra(Comp.CHAMP4, champ4);
        intent.putExtra(Comp.CHAMP5, champ5);
        intent.putExtra(Comp.COMMENT, comment);
    }

    public String toString() {
        return id + COM_SEP + title + COM_SEP + champ1 + COM_SEP + champ2 + COM_SEP + champ3 + COM_SEP + champ4 + COM_SEP + champ5 + COM_SEP + COMMENT;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getChamp1() {
        return champ1;
    }

    public void setChamp1(String champ1) {
        this.champ1 = champ1;
    }

    public String getChamp2() {
        return champ2;
    }

    public void setChamp2(String champ2) {
        this.champ2 = champ2;
    }

    public String getChamp3() {
        return champ3;
    }

    public void setChamp3(String champ3) {
        this.champ3 = champ3;
    }

    public String getChamp4() {
        return champ4;
    }

    public void setChamp4(String champ4) {
        this.champ4 = champ4;
    }

    public String getChamp5() {
        return champ5;
    }

    public void setChamp5(String champ5) {
        this.champ5 = champ5;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
