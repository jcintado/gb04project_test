package es.unex.giiis.asee.wikileagueapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import es.unex.giiis.asee.wikileagueapp.model.Build;
import es.unex.giiis.asee.wikileagueapp.model.Champion;


public class MyAdapterBuilds extends RecyclerView.Adapter<es.unex.giiis.asee.wikileagueapp.MyAdapterBuilds.MyBuildViewHolder> {
    private List<Build> mDataset;

    public interface OnBuildListDeleteInteractionListener{
        public void onBuildListDeleteInteraction(Build build);
    }

    public MyAdapterBuilds.OnBuildListDeleteInteractionListener mDeleteListener;

    public interface OnBuildListEditInteractionListener{
        public void onBuildListEditInteraction(Build build, int position);
    }

    public MyAdapterBuilds.OnBuildListEditInteractionListener mEditListener;

    public static class MyBuildViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView titleBuildView;
        public TextView ChampView;
        public TextView Item1View;
        public TextView Item2View;
        public TextView Item3View;
        public TextView Item4View;
        public TextView Item5View;
        public TextView Item6View;
        public View mView;
        public Build mItem;
        public ImageButton mDeleteButton;
        public ImageButton mEditButton;

        public MyBuildViewHolder(View v) {
            super(v);
            mView=v;
            titleBuildView = v.findViewById(R.id.titleBuildView);
            ChampView = v.findViewById(R.id.ChampView);
            Item1View = v.findViewById(R.id.Item1View);
            Item2View = v.findViewById(R.id.Item2View);
            Item3View = v.findViewById(R.id.Item3View);
            Item4View = v.findViewById(R.id.Item4View);
            Item5View = v.findViewById(R.id.Item5View);
            Item6View = v.findViewById(R.id.Item6View);
            mDeleteButton = v.findViewById(R.id.botonDeleteBuild);
            mEditButton = v.findViewById(R.id.botonEditBuild);
        }
    }

    public MyAdapterBuilds(MyAdapterBuilds.OnBuildListDeleteInteractionListener deleteListener, MyAdapterBuilds.OnBuildListEditInteractionListener editListener) {
        mDataset = new ArrayList<Build>();
        mDeleteListener = deleteListener;
        mEditListener = editListener;
    }

    @Override
    public es.unex.giiis.asee.wikileagueapp.MyAdapterBuilds.MyBuildViewHolder onCreateViewHolder(ViewGroup parent,
                                                                                               int viewType) {
        // create a new view
        // Create new views (invoked by the layout manager)
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.frame_build_list, parent, false);

        return new MyBuildViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyAdapterBuilds.MyBuildViewHolder holder, int position) {
        holder.mItem = mDataset.get(position);
        holder.titleBuildView.setText(mDataset.get(position).getTitle());
        holder.ChampView.setText("Campeón: " + mDataset.get(position).getChamp());
        holder.Item1View.setText(mDataset.get(position).getItem1());
        holder.Item2View.setText(mDataset.get(position).getItem2());
        holder.Item3View.setText(mDataset.get(position).getItem3());
        holder.Item4View.setText(mDataset.get(position).getItem4());
        holder.Item5View.setText(mDataset.get(position).getItem5());
        holder.Item6View.setText(mDataset.get(position).getItem6());
        holder.mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDeleteListener.onBuildListDeleteInteraction(holder.mItem);
            }
        });
        holder.mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditListener.onBuildListEditInteraction(holder.mItem, position);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void swap(List<Build> dataset){
        mDataset = dataset;
        notifyDataSetChanged();
    }
}
