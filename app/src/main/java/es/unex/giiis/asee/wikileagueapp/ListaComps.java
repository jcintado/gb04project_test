package es.unex.giiis.asee.wikileagueapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import es.unex.giiis.asee.wikileagueapp.model.Comp;
import es.unex.giiis.asee.wikileagueapp.roomdb.WikiLeagueDatabase;

public class ListaComps extends AppCompatActivity implements MyAdapterComps.OnCompListDeleteInteractionListener, MyAdapterComps.OnCompListEditInteractionListener {
    private static final int ADD_COMP_REQUEST = 102;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private MyAdapterComps mAdapter;
    ListaCompsVM mViewModel;
    AppContainer appContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_comps);
        FloatingActionButton crearComp = (FloatingActionButton) findViewById(R.id.crearComp);
        crearComp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ListaComps.this, CrearComp.class);
                startActivityForResult(intent,ADD_COMP_REQUEST);
            }
        });
        mRecyclerView = (RecyclerView) findViewById(R.id.compList);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MyAdapterComps(this, this);
        mRecyclerView.setAdapter(mAdapter);

        appContainer = ((WikiLeague) getApplication()).appContainer;
        mViewModel = new ViewModelProvider(this, appContainer.factoryListaComps).get(ListaCompsVM.class);

        mViewModel.getAllComps().observe(this, comps -> {
            mAdapter.swap(comps);
            if (comps != null && comps.size() != 0) showCompsDataView();
            else showLoading();
        });
    }

    private void showLoading(){
        mRecyclerView.setVisibility(View.INVISIBLE);
    }

    private void showCompsDataView(){
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);

        if (requestCode == ADD_COMP_REQUEST){
            if (resultCode == RESULT_OK){
                Comp comp = new Comp(data);
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mViewModel.insert(comp);
                    }
                });
            }
        }
    }

    @Override
    public void onCompListDeleteInteraction(Comp comp, int position) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mViewModel.delete(comp);
            }
        });
    }

    @Override
    public void onCompListEditInteraction(Comp comp, int position) {
        Intent intent = new Intent(ListaComps.this, EditarComp.class);
        intent.putExtra("ID", comp.getId());
        intent.putExtra("title", comp.getTitle());
        intent.putExtra("champ1", comp.getChamp1());
        intent.putExtra("champ2", comp.getChamp2());
        intent.putExtra("champ3", comp.getChamp3());
        intent.putExtra("champ4", comp.getChamp4());
        intent.putExtra("champ5", comp.getChamp5());
        intent.putExtra("comment", comp.getComment());
        startActivity(intent);
    }
}