package es.unex.giiis.asee.wikileagueapp.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "favChamps")
public class FavChamp {

    @Ignore
    public static final String FAV_CHAMP_SEP = System.getProperty("line.separator");
    @Ignore
    public final static String ID = "ID";
    @Ignore
    public final static String NAME = "name";
    @Ignore
    public final static String IMAGE = "image";
    @Ignore
    public final static String DOMINIO = "dominio";

    @PrimaryKey(autoGenerate = true)
    private long id;
    @ColumnInfo(name = "name")
    private String name = new String();
    @ColumnInfo(name = "image")
    private String image = new String();
    @ColumnInfo(name = "dominio")
    private String dominio = new String();

    @Ignore
    public FavChamp(String name, String image, String dominio) {
        this.name = name;
        this.image = image;
        this.dominio = dominio;
    }

    public FavChamp(long id, String name, String image, String dominio) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.dominio = dominio;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String toString() {
        return id + FAV_CHAMP_SEP + name;
    }

    public String getDominio() { return dominio; }

    public void setDominio(String dominio) { this.dominio = dominio; }
}
