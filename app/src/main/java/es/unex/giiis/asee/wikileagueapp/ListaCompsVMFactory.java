package es.unex.giiis.asee.wikileagueapp;

import android.content.Context;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class ListaCompsVMFactory extends ViewModelProvider.NewInstanceFactory {
    private final RoomRepository mRepository;
    private final Context context;

    public ListaCompsVMFactory(Context context, RoomRepository repository) {
        this.mRepository = repository;
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new ListaCompsVM(context, mRepository);
    }
}
