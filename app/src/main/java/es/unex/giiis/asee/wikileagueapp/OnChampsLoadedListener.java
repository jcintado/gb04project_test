package es.unex.giiis.asee.wikileagueapp;

import es.unex.giiis.asee.wikileagueapp.model.Champion;

import java.util.List;


public interface OnChampsLoadedListener {
    public void onChampsLoaded(List<Champion> champs);
}
