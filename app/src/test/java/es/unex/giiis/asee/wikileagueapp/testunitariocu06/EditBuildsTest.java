package es.unex.giiis.asee.wikileagueapp.testunitariocu06;


import android.content.Intent;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import es.unex.giiis.asee.wikileagueapp.model.Build;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class EditBuildsTest {

    public static Build b1;
    public static Build b2;

    @BeforeClass
    public static void initClass() {
        b1 = new Build(1, "", "", "", "", "", "", "", "");
        b1.setId(1);
        b1.setTitle("Items");
        b1.setChamp("NASUS");
        b1.setItem1("Escoba");
        b1.setItem2("Fregona");
        b1.setItem3("Olla_Express");
        b1.setItem4("Estropajo");
        b1.setItem5("Sarten");
        b1.setItem6("Thermomix");
    }

    @Test
    public void testBuilds() {
        assertNotNull(b1);
    }

    @Test
    public void testBuilds2() {
        assertNull(b2);
    }

    @Test
    public void testitle() {
        assertNotNull(b1.getTitle());
        assertNotEquals(b1.getTitle(), "titems");
        assertEquals(b1.getTitle(), "Items");
    }

    @Test
    public void teschamp() {
        assertNotNull(b1.getChamp());
        assertNotEquals(b1.getChamp(), "NISUS");
        assertEquals(b1.getChamp(), "NASUS");
    }

    @Test
    public void testid() {
        assertNotNull(b1.getId());
        assertNotEquals(b1.getId(), 3);
        assertEquals(b1.getId(), 1);
    }

    @Test
    public void testitem1() {
        assertNotNull(b1.getItem1());
        assertNotEquals(b1.getItem1(), "pirulet");
        assertEquals(b1.getItem1(), "Escoba");
    }

    @Test
    public void testitem2() {
        assertNotNull(b1.getItem2());
        assertNotEquals(b1.getItem2(), "piruleta");
        assertEquals(b1.getItem2(), "Fregona");
    }

    @Test
    public void testitem3() {
        assertNotNull(b1.getItem3());
        assertNotEquals(b1.getItem3(), "piruleta");
        assertEquals(b1.getItem3(), "Olla_Express");
    }

    @Test
    public void testitem4() {
        assertNotNull(b1.getItem4());
        assertNotEquals(b1.getItem4(), "piruleta");
        assertEquals(b1.getItem4(), "Estropajo");
    }

    @Test
    public void testitem5() {
        assertNotNull(b1.getItem5());
        assertNotEquals(b1.getItem5(), "piruleta");
        assertEquals(b1.getItem5(), "Sarten");
    }

    @Test
    public void testitem6() {
        assertNotNull(b1.getItem6());
        assertNotEquals(b1.getItem6(), "piruleta");
        assertEquals(b1.getItem6(), "Thermomix");
    }

    @AfterClass
    public static void finishClass() {
        b1 = null;
    }   

}
