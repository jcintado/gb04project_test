package es.unex.giiis.asee.wikileagueapp.testunitariocu12;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import es.unex.giiis.asee.wikileagueapp.model.Comp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class AddCompTest {

    public static Comp c1;
    public static Comp c2;

    @BeforeClass
    public static void initClass() {
        c1 = new Comp(0, "", "", "", "", "", "", "");
        c1.setId(1);
        c1.setTitle("Composicion1");
        c1.setChamp1("NASUS");
        c1.setChamp2("RENGAR");
        c1.setChamp3("AHRI");
        c1.setChamp4("ASHE");
        c1.setChamp5("JANNA");
        c1.setComment("Buena composicion");
    }

    @Test
    public void testComps() {
        assertNotNull(c1);
    }

    @Test
    public void testComps2() {
        assertNull(c2);
    }

    @Test
    public void testitle() {
        assertNotNull(c1.getTitle());
        assertNotEquals(c1.getTitle(), "compos");
        assertEquals(c1.getTitle(), "Composicion1");
    }

    @Test
    public void teschamp1() {
        assertNotNull(c1.getChamp1());
        assertNotEquals(c1.getChamp1(), "NISUS");
        assertEquals(c1.getChamp1(), "NASUS");
    }

    @Test
    public void teschamp2() {
        assertNotNull(c1.getChamp2());
        assertNotEquals(c1.getChamp2(), "NISUS");
        assertEquals(c1.getChamp2(), "RENGAR");
    }

    @Test
    public void teschamp3() {
        assertNotNull(c1.getChamp3());
        assertNotEquals(c1.getChamp3(), "NISUS");
        assertEquals(c1.getChamp3(), "AHRI");
    }

    @Test
    public void teschamp4() {
        assertNotNull(c1.getChamp4());
        assertNotEquals(c1.getChamp4(), "NISUS");
        assertEquals(c1.getChamp4(), "ASHE");
    }

    @Test
    public void teschamp5() {
        assertNotNull(c1.getChamp5());
        assertNotEquals(c1.getChamp5(), "NISUS");
        assertEquals(c1.getChamp5(), "JANNA");
    }

    @Test
    public void tescomment() {
        assertNotNull(c1.getComment());
        assertNotEquals(c1.getComment(), "NISUS");
        assertEquals(c1.getComment(), "Buena composicion");
    }

    @AfterClass
    public static void finishClass() {

        c1 = null;

    }

}
