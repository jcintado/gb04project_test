package es.unex.giiis.asee.wikileagueapp.testunitariocu11;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import es.unex.giiis.asee.wikileagueapp.model.Event;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class AddEventTest {

    public static Event evento1;
    public static Event evento2;

    @BeforeClass
    public static void initClass(){
        evento1 = new Event(0 , "", "");
        evento1.setId(1);
        evento1.setTitle("Streaming en Youtube");
        evento1.setBody("Suscribete y dale al like");
    }

    @Test
    public void testEvento1(){
        assertNotNull(evento1);
    }

    @Test
    public void testEvento2(){
        assertNull(evento2);
    }

    @Test
    public void testId(){

        assertNotNull(evento1.getId());
        assertNotEquals(evento1.getId(),2);
        assertEquals(evento1.getId(),1);
    }

    @Test
    public void testTitle(){

        assertNotNull(evento1.getTitle());
        assertNotEquals(evento1.getTitle(),"Piruleta");
        assertEquals(evento1.getTitle(),"Streaming en Youtube");
    }

    @Test
    public void testBody(){

        assertNotNull(evento1.getBody());
        assertNotEquals(evento1.getBody(),"Piruleta");
        assertEquals(evento1.getBody(),"Suscribete y dale al like");
    }

    @AfterClass
    public static void finishClass (){
        evento1 = null;
    }

}
