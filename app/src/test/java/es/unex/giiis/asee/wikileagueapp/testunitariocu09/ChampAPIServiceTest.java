package es.unex.giiis.asee.wikileagueapp.testunitariocu09;



import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.wikileagueapp.ChampsServices;
import es.unex.giiis.asee.wikileagueapp.model.Champion;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ChampAPIServiceTest {

    @Test
    public void getChampionTest() throws IOException {

        //Step1: we create champion list
        Champion champ = new Champion();
        champ.setArmor(30.0);
        champ.setArmorperlevel(2.0);
        champ.setAttackdamage(60.0);
        champ.setAttackdamageperlevel(3.0);
        champ.setAttackrange(550.0);

        champ.setAttackspeedperlevel(10.0);
        champ.setBigImageUrl("hola.com");
        champ.setCrit(10.0);
        champ.setCritperlevel(0.0);
        champ.setHp(475.0);
        champ.setHpperlevel(15.0);
        champ.setHpregen(8.5);
        champ.setHpregenperlevel(2.0);
        champ.setId(1);
        champ.setImageUrl("hola.com/campeon");

        champ.setMovespeed(375.0);
        champ.setMp(350.0);
        champ.setMpperlevel(25.0);
        champ.setMpregen(15.0);
        champ.setMpregenperlevel(5.5);
        champ.setName("Ezreal");
        champ.setSpellblock(45.0);
        champ.setSpellblockperlevel(3.5);


        //Champion dos
        Champion champ2 = new Champion();

        champ2.setArmor(20.0);
        champ2.setArmorperlevel(3.0);
        champ2.setAttackdamage(65.0);
        champ2.setAttackdamageperlevel(3.0);
        champ2.setAttackrange(650.0);

        champ2.setAttackspeedperlevel(10.0);
        champ2.setBigImageUrl("hola2.com");
        champ2.setCrit(0.0);
        champ2.setCritperlevel(0.0);
        champ2.setHp(485.0);
        champ2.setHpperlevel(9.0);
        champ2.setHpregen(8.5);
        champ2.setHpregenperlevel(2.0);
        champ2.setId(2);
        champ2.setImageUrl("hola2.com/campeon");

        champ2.setMovespeed(355.0);
        champ2.setMp(450.0);
        champ2.setMpperlevel(15.0);
        champ2.setMpregen(10.0);
        champ2.setMpregenperlevel(5.5);
        champ2.setName("Ashe");
        champ2.setSpellblock(40.0);
        champ2.setSpellblockperlevel(7.5);

        List<Champion> champList = new ArrayList<>();
        champList.add(champ);
        champList.add(champ2);

        //Step2: Define a mapper Object to JSON
        ObjectMapper objectMapper = new ObjectMapper();

        //Step3: we create a mock server, independent of the real server
        MockWebServer mockWebServer = new MockWebServer();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Step4: we create a standard response to any request (in this case, only getCenters request will be performed)
        MockResponse mockedResponse = new MockResponse();
        mockedResponse.setResponseCode(200);
        mockedResponse.setBody(objectMapper.writeValueAsString(champList));
        mockWebServer.enqueue(mockedResponse);

        //Step5: we link the mock server with our retrofit api
        ChampsServices service = retrofit.create(ChampsServices.class);



        //Step6: we create the call to get centers
        Call<List<Champion>> call = service.listChamps();
        // and we execute the call
        Response<List<Champion>> response = call.execute();

        //Step7: let's check that the call is executed
        assertTrue(response != null);
        assertTrue(response.isSuccessful());

        //Step8bis: check the body content
        List<Champion> championsResponse =response.body();
        assertFalse(championsResponse.isEmpty());
        assertTrue(championsResponse.size()==2);

        assertEquals(champ.getArmor(), 30.0, 0.0);
        assertEquals(champ.getArmorperlevel(), 2.0, 0.0);
        assertEquals(champ.getAttackdamage(), 60.0, 0.0);
        assertEquals(champ.getAttackdamageperlevel(), 3.0, 0.0);
        assertEquals(champ.getAttackrange(), 550.0, 0.0);
        assertEquals(champ.getAttackspeedperlevel(), 10.0, 0.0);
        assertEquals(champ.getBigImageUrl(), "hola.com");
        assertEquals(champ.getCrit(), 10.0,0.0);
        assertEquals(champ.getCritperlevel(), 0.0, 0.0);
        assertEquals(champ.getHp(), 475.0, 0.0);
        assertEquals(champ.getHpperlevel(), 15.0, 0.0);
        assertEquals(champ.getHpregen(), 8.5, 0.0);
        assertEquals(champ.getHpregenperlevel(), 2.0, 0.0);
        assertEquals(champ.getId(), new Integer(1));
        assertEquals(champ.getImageUrl(), "hola.com/campeon");
        assertEquals(champ.getMovespeed(), 375.0, 0.0);
        assertEquals(champ.getMp(), 350.0, 0.0);
        assertEquals(champ.getMpperlevel(), 25.0, 0.0);
        assertEquals(champ.getMpregen(), 15.0, 0.0);
        assertEquals(champ.getMpregenperlevel(), 5.5, 0.0);
        assertEquals(champ.getName(), "Ezreal");
        assertEquals(champ.getSpellblock(), 45.0,0.0);
        assertEquals(champ.getSpellblockperlevel(), 3.5, 0.0);



        //assertTrue(championsResponse.contains(champ2));

        assertEquals(champ2.getArmor(), 20.0, 0.0);
        assertEquals(champ2.getArmorperlevel(), 3.0, 0.0);
        assertEquals(champ2.getAttackdamage(), 65.0, 0.0);
        assertEquals(champ2.getAttackdamageperlevel(), 3.0, 0.0);
        assertEquals(champ2.getAttackrange(), 650.0, 0.0);
        assertEquals(champ2.getAttackspeedperlevel(), 10.0, 0.0);
        assertEquals(champ2.getBigImageUrl(), "hola2.com");
        assertEquals(champ2.getCrit(), 0.0,0.0);
        assertEquals(champ2.getCritperlevel(), 0.0, 0.0);
        assertEquals(champ2.getHp(), 485.0, 0.0);
        assertEquals(champ2.getHpperlevel(), 9.0, 0.0);
        assertEquals(champ2.getHpregen(), 8.5, 0.0);
        assertEquals(champ2.getHpregenperlevel(), 2.0, 0.0);
        assertEquals(champ2.getId(), new Integer(2));
        assertEquals(champ2.getImageUrl(), "hola2.com/campeon");
        assertEquals(champ2.getMovespeed(), 355.0, 0.0);
        assertEquals(champ2.getMp(), 450.0, 0.0);
        assertEquals(champ2.getMpperlevel(), 15.0, 0.0);
        assertEquals(champ2.getMpregen(), 10.0, 0.0);
        assertEquals(champ2.getMpregenperlevel(), 5.5, 0.0);
        assertEquals(champ2.getName(), "Ashe");
        assertEquals(champ2.getSpellblock(), 40.0,0.0);
        assertEquals(champ2.getSpellblockperlevel(), 7.5, 0.0);


        //Step9: Finish web server
        mockWebServer.shutdown();
    }



    @Test
    public void getEmptyChampionsTest() throws IOException {

        //Step1: we create empty champions list
        List<Champion> champions = new ArrayList<>();


        //Step2: Define a mapper Object to JSON
        ObjectMapper objectMapper = new ObjectMapper();

        //Step3: we create a mock server, independent of the real server
        MockWebServer mockWebServer = new MockWebServer();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Step4: we create a standard response to any request (in this case, only get list champions request will be performed)
        MockResponse mockedResponse = new MockResponse();
        mockedResponse.setResponseCode(200);
        mockedResponse.setBody(objectMapper.writeValueAsString(champions));
        mockWebServer.enqueue(mockedResponse);

        //Step5: we link the mock server with our retrofit api
        ChampsServices service = retrofit.create(ChampsServices.class);



        //Step6: we create the call to get champions
        Call<List<Champion>> call = service.listChamps();
        // and we execute the call
        Response<List<Champion>> response = call.execute();

        //Step7: let's check that the call is executed
        assertTrue(response != null);
        assertTrue(response.isSuccessful());

        //Step8bis: check the body content
        List<Champion> championsResponse =response.body();
        assertTrue(championsResponse.isEmpty());

        //Step9: Finish web server
        mockWebServer.shutdown();
    }

}

